# [General Skills] [Easy] - Netcat
## Description
Simple introduction to test netcat

## Author(s)
- RPScylla

## ToDo:
- Fix flag
- Fix CTFd text

## How to build/host
- Docker image
- Text instructions on CTFd
- Hint on CTFd

### Docker compile commands
```
MultiStage docker pipeline (source is build in first image)

sudo docker build --rm -t netcat .
sudo docker run -p 1234:1234 --rm -d netcat
```

### Text instructions
> You can connect to other services using `netcat` at `ip:port`

### Hint
> Try connecting to the service

## Solution
<details>
<summary>Click to show</summary>
<p>

- Connect using netcat

</p>
</details>

### Flag
```PTH{netcat_is_easy}```
