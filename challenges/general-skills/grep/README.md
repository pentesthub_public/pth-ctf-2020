# [General Skills] [Easy] - Grep
## Description
Simple introduction to test grep

## Author(s)
- RPScylla

## ToDo:
- Fix flag
- Fix CTFd text

## How to build/host
- File on CTFd
- Text instructions on CTFd
- Hint on CTFd

### File on CTFd
- `file.txt`

### Text instructions
> somewhere in this file is the flag?

### Hint
> Try filtering using grep

## Solution
<details>
<summary>Click to show</summary>
<p>

- `grep PTH{ file.txt`

</p>
</details>

### Flag
```PTH{grep_makes_looking_easy}```
