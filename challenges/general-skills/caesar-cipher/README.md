# [Crypto] [Easy] - Caesar Cipher
## Description
Simple caesar cipher challenge.

## Author(s)
- RPScylla

## ToDo:
- Include better flag
- Include better CTFd text

## How to build/host
- Text instructions on CTFd
- Hint on CTFd

### Text instructions
```
To Generate the flag change the `flag` file and run `./generate.py`  output is saved in `output.txt`
```
> We found this `HLZ{uswksj14_xdsy}` weird corupted package. We are sure this is something the firewall messed up. Can u figure out what it should be?

### Hint
> Caesar was a wise man.

## Solution
<details>
<summary>Click to show</summary>
<p>

see `poc.py`

</p>
</details>

### Flag
```PTH{caesar14_flag}```