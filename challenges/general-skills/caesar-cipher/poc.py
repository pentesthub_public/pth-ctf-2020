#!/usr/bin/python3
import string

def caesar(plaintext, shift):
    alphabetLower = string.ascii_lowercase
    alphabetUpper = string.ascii_uppercase
    shifted_alphabetLower = alphabetLower[shift:] + alphabetLower[:shift]
    shifted_alphabetUpper = alphabetUpper[shift:] + alphabetUpper[:shift]
    tableLower = str.maketrans(alphabetLower, shifted_alphabetLower)
    tableUpper = str.maketrans(alphabetUpper, shifted_alphabetUpper)
    return plaintext.translate(tableLower).translate(tableUpper)

with open('output.txt', 'r') as caesarFile:
    caesarInput = caesarFile.read()
    for x in range (0, 28):
        caesarOutput = caesar(caesarInput, x)
        if caesarOutput.startswith('PTH'):
            print (caesarOutput)
