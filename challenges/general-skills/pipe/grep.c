#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void print_random_string(size_t size)
{
    const char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789{}_\n";
    for (size_t n = 0; n < size; n++) {
        int key = rand() % (int) (sizeof charset - 1);
        printf("%c",charset[key]);
    }
}

void main(int argc, char* argv[]){
    setvbuf(stdout, NULL, _IONBF, 0);
    print_random_string(8324);
    printf("\n");
    system("cat flag");
    printf("\n");
    print_random_string(2137);
    exit(0);
}