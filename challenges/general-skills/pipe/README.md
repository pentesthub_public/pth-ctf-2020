# [general-skills] [Easy] - Pipe
## Description
Pipe the output of nc in grep

## Author(s)
- RPScylla

## ToDo:
- Fix flag
- Fix text on CTFD

## How to build/host
- Docker image
- Text instructions on CTFd
- Hint on CTFd

### Docker compile commands
```
MultiStage docker pipeline (source is build in first image)

sudo docker build --rm -t grep .
sudo docker run -p 1234:1234 --rm -d grep
```

### Text instructions
> todo

### Hint
> plummers use pipes

## Solution
<details>
<summary>Click to show</summary>
<p>

- `nc localhost 1234 | grep PTH{`

</p>
</details>

### Flag
```PTH{grep}```
