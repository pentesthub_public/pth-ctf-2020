#!/usr/bin/python3
import base64

with open('output.txt', 'r') as base64File:
    base64_bytes = base64File.read().encode('utf-8')
    flag = base64_bytes
    while True:
        flag = base64.b64decode(flag)
        if(str(flag, 'utf-8').startswith("PTH")):
            print(str(flag, 'utf-8'))
            break
