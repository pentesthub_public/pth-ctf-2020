# [Crypto] [Easy] - base64
## Description
Simple base64 warmup challenge.

## Author(s)
- RPScylla

## ToDo:
- Include better flag
- Include better CTFd text

## How to build/host
- Text instructions on CTFd
- Hint on CTFd
- File on CTFd

### Text instructions
```
To Generate the flag change the `flag` file and run `./generate.py` output is saved in `output.txt`
```
> We found this weird package. We are sure a component of the firewall is not working correctly. Can u figure out what it should be?

### File on CTFd
- `output.txt`

### Hint
> It looks like some sort of binary to text encoding.

## Solution
<details>
<summary>Click to show</summary>
<p>

see `poc.py`

</p>
</details>

### Flag
```PTH{B4s3_64_1S_N0_3ncRYp710N}```