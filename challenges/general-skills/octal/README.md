# [Crypto] [Easy] - Octal
## Description
Simple octal warmup challenge.

## Author(s)
- RPScylla

## ToDo:
- Include better CTFd text

## How to build/host
- Text instructions on CTFd
- Hint on CTFd

### Text instructions
```
To Generate the flag change the `flag` file and run `./generate.py` output is saved in `output.txt`
```
> We found this `120 124 110 173 60 143 67 60 160 125 163 137 103 60 125 156 67 123 137 154 61 153 63 137 67 150 61 163 175` weird string of numbers. We cant read what it should say. Can u figure out what it should be?

### Hint
> Why is the highest number only 7?

## Solution
<details>
<summary>Click to show</summary>
<p>

see `poc.py`

</p>
</details>

### Flag
```PTH{0c70pUs_C0Un7S_l1k3_7h1s}```