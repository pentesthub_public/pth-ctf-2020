#!/usr/bin/python3

import string
import random
import os, shutil
from zipfile import ZipFile


def random_string(length):
        def random_char():
            return random.choice(string.ascii_letters + string.digits)
        return ''.join(random_char() for _ in range(length))

with open('flag', 'r') as flagFile:
    flag = flagFile.read()    

first = True

zipObj = ZipFile('files.zip', 'w')
genDir = "files"
if (os.path.exists(genDir)):
        shutil.rmtree(genDir)
os.mkdir(genDir)

for x in range(0, 18):
    dirXname = random_string(random.randint(10,31))
    if not (os.path.exists(genDir+'/'+dirXname)):
        os.mkdir(genDir+'/'+dirXname)

    for y in range(0, random.randint(6,10)):
        dirYname = random_string(random.randint(6,12))
        if not (os.path.exists(genDir+'/'+dirXname+'/'+dirYname)):
            os.mkdir(genDir+'/'+dirXname+'/'+dirYname)
    
        for z in range (0, random.randint(2,6)):
            dirZname = random_string(random.randint(6,12))
            if not (os.path.exists(genDir+'/'+dirXname+'/'+dirYname+'/'+dirZname)):
                os.mkdir(genDir+'/'+dirXname+'/'+dirYname+'/'+dirZname)
    
            for t in range (0, random.randint(4,8)):
                filepath = genDir+'/'+dirXname+'/'+dirYname+'/'+dirZname+'/'+random_string(random.randint(6,12))+".txt"
                with open(filepath,"w+") as outputFile:
                    if first:
                        outputFile.write(flag)
                        first = False
                    else:
                        outputFile.write(random_string(random.randint(10,30)))
                zipObj.write(filepath)


