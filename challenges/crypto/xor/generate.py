#!/usr/bin/python3

def xor(msg, key):
    o = ''
    for i in range(len(msg)):
        o += chr(ord(msg[i]) ^ ord(key[i % len(key)]))
    return o

with open('flag', 'r') as messageFile:
    message = messageFile.read()

with open('key', 'r') as k:
    key = ''.join(k.readlines()).rstrip('\n')

assert key.isalnum() and (len(key) == 5)
assert ((len(message) % 5) == 0)

with open ("encrypted","w+") as outputFile:
    outputFile.write(xor(message, key))
