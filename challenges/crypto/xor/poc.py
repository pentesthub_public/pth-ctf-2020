#!/usr/bin/python3

import string

def xor(msg, key):
    o = ''
    for i in range(len(msg)):
        o += chr(ord(msg[i]) ^ ord(key[i % len(key)]))
    return o
  
with open('encrypted', 'r') as f:
    msg = ''.join(f.readlines())

first_part_of_msg = 'PTH{'
first_part_candidates = []

for i in range(len(msg)):
    candidate = xor(msg[i:i+len(first_part_of_msg)], first_part_of_msg)
    if candidate.isalnum():
        first_part_candidates.append(candidate)

last_part_of_msg = '}'
last_part_candidates = []

for i in range(len(msg)):
    candidate = xor(msg[i:i+len(last_part_of_msg)], last_part_of_msg)
    if candidate.isalnum():
        last_part_candidates.append(candidate)

for first_part in first_part_candidates:
    for last_part in last_part_candidates:
        decrypt = (xor(msg, first_part+last_part))
        if(decrypt.startswith(first_part_of_msg) and decrypt.endswith(last_part_of_msg)):
            print(decrypt)

