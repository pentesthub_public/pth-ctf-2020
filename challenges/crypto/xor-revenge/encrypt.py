import base64

data = open("data.txt", "r").read()
filtered = data.replace("\n", " ")
key = "PTF{5h0uld-hAvE-U5eD-A-0ne-T1ME-Pad}"

def xor(m, k):
    s = ""
    for i in range(len(m)):
        s += chr(ord(m[i]) ^ ord(k[i % len(k)]))
    return s

encrypted = str(base64.b64encode(bytes((xor(filtered, key)), "utf-8")), "utf-8")
with open('out.txt', 'w+') as outFile:
    outFile.write(encrypted)
