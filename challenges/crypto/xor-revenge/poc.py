import base64
import numpy as np

def xor(m, k):
    s = ""
    for i in range(len(m)):
        s += chr(ord(m[i]) ^ ord(k[i % len(k)]))
    return s

def distance(x, y):
    s = 0
    for i in range(len(x)):
        a = ord(x[i])
        b = ord(y[i])
        c = a^b
        for c in bin(c):
            if c == "1":
                s += 1

    return s

data = base64.b64decode(open("out.txt", "r").read())
nums = []
for x in data:
    nums.append(x)

def splitArray(arr, num):
    l = len(arr) // num
    result = []
    for x in range(num):
        result.append(arr[x*l:(x + 1)*l])
    return result


def findKeyLength():
    scores = {}
    for x in range(2, 40):
        s = str(data, "utf-8")
        arrs = splitArray(s, len(s) // x)
        arrs = arrs[:10]
        distances = []
        for i in range(len(arrs)):
            distances.append(distance(arrs[i], arrs[(i + 1) % len(arrs)]) / len(arrs[0]))
        scores[x] = sum(distances) / len(distances)
    return min(scores, key=lambda x: scores[x])

letterfreq = {
"E": 0.111607,
"M": 0.030129,
"A": 0.084966,
"H": 0.030034,
"R": 0.075809,
"G": 0.024705,
"I": 0.075448,
"B": 0.020720,
"O": 0.071635,
"F": 0.018121,
"T": 0.069509,
"Y": 0.017779,
"N": 0.066544,
"W": 0.012899,
"S": 0.057351,
"K": 0.011016,
"L": 0.054893,
"V": 0.010074,
"C": 0.045388,
"X": 0.002902,
"U": 0.036308,
"Z": 0.002722,
"D": 0.033844,
"J": 0.001965,
"P": 0.031671,
"Q": 0.001962,
" ": 0.18288
}
def scoreString(s):
    s = s.upper()
    scores = {}
    score = 0
    for c in s:
        if not c in scores:
            scores[c] = 1
        else:
            scores[c] += 1
    for c in letterfreq:
        if c in scores:
            score += abs(letterfreq[c] - (scores[c] / len(s)))
        else:
            score += letterfreq[c]
    return score

length = findKeyLength()
key = ""
for x in range(length):
    arr = str(data, "utf-8")[x::length]
    scores = {}
    for y in range(256):
        xorred = xor(arr, [chr(y)])
        score = scoreString(xorred)
        scores[chr(y)] = score
    key += min(scores, key=lambda x: scores[x])
    print(min(scores, key=lambda x: scores[x]), end="")
