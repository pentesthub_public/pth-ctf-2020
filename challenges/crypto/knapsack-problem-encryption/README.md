# [crypto] [Hard] - Knapsacks
## Description
Makes use of the [Merkle–Hellman knapsack cryptosystem.](https://en.wikipedia.org/wiki/Merkle%E2%80%93Hellman_knapsack_cryptosystem)
Has been found to be vulnerable by [Shamir, Adi (1984).](https://doi.org/10.1109%2FSFCS.1982.5). 
Proof of concept makes use of [Lattice Reduction (LLL)]

## Author(s)
- PrincePanda

## ToDo:
- additional testing

## How to build/host
- Text instructions on CTFd
- Hint on CTFd
- Files on CTFd

### Text instructions
> Did you know you can use knapsacks to encrypt your data?
> I encrypted my flag using my favourite knapsack can you get it back?

### File on CTFd
`out.txt`
`cipher.py`

### Hint
> It's all "L": Lenstra Lenstra Lovász Lattice

## Solution
Use lattice reduction to recover the plaintext.
Other approaches are possible too.
PoC contains code to solve the challenge.

### Flag
```PTF{Kn4p54Ck_4TT4cX}```
