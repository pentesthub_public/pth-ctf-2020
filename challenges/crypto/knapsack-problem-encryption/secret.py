from random import Random
from Crypto.Util import number
from math import gcd

flagtext = "PTF{Kn4p54Ck_4TT4cX}"
flag = "0101000001010100010001100111101101001011011011100011010001110000001101010011010001000011011010110101111100110100010101000101010000110100011000110101100001111101"

def privateNums():
    nums = []
    for x in range(160):
        nums.append(sum(nums) + Random().randint(0, 1000))
    return nums


print("generating private nums")

private_nums = privateNums()

print("generated private nums")

def getN():
    n = 0
    i = 0
    while n < sum(private_nums):
        i += 1
        n = number.getPrime(i)
    return n


print("generating n")

n = getN()

print("generated n")

def getM():
    m = Random().randint(0, n)
    while (gcd(m, n) != 1):
        m = Random().randint(0, n)
    return m


print("generating m")

m = getM()

print("generated m")

private_key = (private_nums, m, n)

def publicNums():
    pubs = []
    for x in private_nums:
        pubs.append((x * m) % n)
    return pubs


print("generating public nums")

public_key = publicNums()

print("generated public nums")

