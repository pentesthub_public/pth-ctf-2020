#!/usr/bin/python3

import encryptLib

with open('flag', 'r') as flagFile:
    flag = flagFile.read()

with open ("encrypted","w+") as outputFile:
    outputFile.write(encryptLib.encrypt(flag))
