#!/usr/bin/python3
def shift(input, amount):
    return input[amount:] + input[:amount]

def interleave(input):
    retval = ""
    for i in range(0, len(input), 2):
        retval += input[i]
    for i in range(1, len(input), 2):
        retval += input[i]
    return retval

def mirror(input):
    return input[::-1]

def increment(input, amount):
    retval = ""
    for i in range(0, len(input)):
        retval += chr(ord(input[i])+amount)
    return retval

def encrypt(input):
    input = increment(input, 3)
    for i in range(4):
        for j in range(3):
            input = shift(input, int(len(input)/(j+1)))
            input = mirror(input)
            input = interleave(input)
            input = shift(input, int(len(input)/(i+j+1)))
        for j in range(2):
            input = shift(input, int(len(input)/(j+1)))
            input = interleave(input)
            input = mirror(input)
    return input