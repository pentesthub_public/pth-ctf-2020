#!python

from Crypto.Util import number
from Crypto.Hash import MD5 
import string 
import random
import sympy
import multiprocessing
import multiprocessing.queues
import time

def lcm(x, y):
    from math import gcd
    return x * y // gcd(x, y)

bits = 128

manager = multiprocessing.Manager()
queue = manager.Queue(1)

def tryReduce(phi, n):
    factors = sympy.ntheory.factorint(phi, multiple=True)
    a = number.getPrime(20)
    queue.put(min(reduceOrder(a, phi, n, factors)))

def reduceOrder(a, m, n, factors):
    options = [m]
    for p in factors:
        if (pow(a, m // p, n) == 1):
            options.extend(reduceOrder(a, m // p, n, factors))
    return options

def singleChall():
    p = number.getPrime(bits // 2)
    q = number.getPrime(bits // 2)
    e = 0x10001
    n = p * q
    plaintext = random.randint(n // 2, n)
    c = pow(plaintext, e, n)
    phi = lcm(p-1,q-1)
    print("** Reducing Order Please Wait **")
    
    proc = multiprocessing.Process(target=tryReduce, args=[phi, n])
    proc.start()
    proc.join(4)
    if proc.is_alive():
        proc.terminate()
        proc.join()
        reduced = phi
    else:
        reduced = queue.get()

    print("n: " + str(n))
    print("e: " + str(e))
    print("c: " + str(c))
    print("order of a^x mod n: " + str(reduced))
    try:
        guess = int(input("Please provide the plaintext: "))
        assert(guess == plaintext)
        print("Well done!")
        return True
    except:
        print("Wrong :( try again")
        return False

def proof_of_work(hardness):
    prefix = "".join([string.hexdigits[random.randint(0, 16)] for _ in range(hardness)]).upper()
    print("Please provide hex input such that md5(input)[:" + str(hardness) + "] = " + prefix)
    i = input().upper()
    try:
        out = MD5.MD5Hash(bytes.fromhex(i)).hexdigest().upper()
    except:
        print("Please provide a valid hexadecimal string")
        return False
    return out[:hardness] == prefix


while True:
    res = proof_of_work(6)
    if res: break
    print("wrong!")

rounds = 10
for i in range(rounds):
    if not singleChall():
        exit()

print("Well done the order has been restored, Here's your flag: PTH{5h0R5_0RD3R_R3DUC7i0N}")

