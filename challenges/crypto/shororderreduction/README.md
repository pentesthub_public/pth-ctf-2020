# [crypto] [Medium] - Reduce the order
## Description
RSA decryption challenge which can be solved using the given order of 
```math
a^x\ mod\ n
``` 
as from Shor's paper

## Author(s)
- PrincePanda

### Text instructions
> I was told restore the order in the chaos of crypto.
> I'm not sure if i fully understand what they meant.
> Help me with these problems and i'll give you a flag


## How to build/host
- Docker image
- Text instructions on CTFd
- Hint on CTFd

### Docker compile commands
```
sudo docker build --rm -t shorsorderreduction .
sudo docker run -d --rm -p 1234:1234 shorsorderreduction
```


### Hint
> I've heard quantum computers are really good at finding order

## Solution
As per Shor's paper RSA can be broken by solving the problem of order finding.
This means that when given the order of a function `a^x mod n` we can efficiently factorize n.
Since the order is given we can thus factorize N, create the private key and decrypt the message.

### Flag
```PTH{5h0R5_0RD3R_R3DUC7i0N}```
