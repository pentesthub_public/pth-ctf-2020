import socket
import sys
import _thread
import time
import math
from enum import Enum
from server.generators import stages_generator
import server.config as c


class StateCodes(Enum):
    NONE = 0
    TIME_UP = 1
    INCORRECT_ANSWER = 2
    CORRECT_ANSWER = 3
    FINISHED = 4


def client_thread(connection):
    now = math.floor(time.time())
    end = now + c.ROUND_DURATION
    stage = 0
    running = True
    state = StateCodes.NONE

    if callable(c.STAGE_COUNT):
        stages = stages_generator(c.STAGE_COUNT())
    else:
        stages = stages_generator(c.STAGE_COUNT)

    while running:
        if len(stages) == stage:
            state = StateCodes.FINISHED
            break

        # send Question
        connection.sendall(stages[stage]['question'].encode('utf-8'))

        # receive answer
        data = connection.recv(1024).decode('utf-8').replace('\n', '')
        # check if the user hasn't been idling for to long
        if end <= math.floor(time.time()):
            state = StateCodes.TIME_UP
            break
        # validate answer
        if data == stages[stage]['result']:
            connection.sendall(b"Correct\n")
            # next round
            stage += 1
            # reset timer
            now = math.floor(time.time())
            end = now + c.ROUND_DURATION
            state = StateCodes.CORRECT_ANSWER
        else:
            running = False
            state = StateCodes.INCORRECT_ANSWER

    # give an reply to the user based upon the last status code set
    if state == StateCodes.NONE:
        connection.sendall(b"Something went wrong, contact an administrator.\n")
    elif state == StateCodes.TIME_UP:
        connection.sendall(b"You were to slow.\n")
    elif state == StateCodes.INCORRECT_ANSWER:
        connection.sendall(b"Incorrect answer.\n")
    elif state == StateCodes.CORRECT_ANSWER:
        connection.sendall(b"Something went wrong, contact an administrator.\n")
    elif state == StateCodes.FINISHED:
        connection.sendall(f"The flag is: {c.FLAG}.\n".encode('utf-8'))

    # terminate connection
    connection.shutdown(2)
    connection.close()


def run():
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    except socket.error as msg:
        print("Could not create socket. Error Code: ", str(msg.errno), "Error: ", msg.strerror)
        sys.exit(0)

    print("[-] Socket Created")

    # bind socket
    try:
        s.bind((c.HOST, c.PORT))
        print("[-] Socket Bound to port " + str(c.PORT))
    except socket.error as msg:
        print("[-] Bind Failed. Error Code: {} Error: {}".format(str(msg.errno), msg.strerror))
        sys.exit()

    s.listen(c.CONNECTIONS)
    print("[-] Listening...")
    while True:
        # blocking call, waits to accept a connection
        conn, addr = s.accept()
        print("[-] Connected to " + addr[0] + ":" + str(addr[1]))

        _thread.start_new_thread(client_thread, (conn,))

    s.close()


if __name__ == '__main__':
    run()
