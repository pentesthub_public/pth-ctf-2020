import math
import random
import hashlib
import string
import base64
from server.config import DEBUG, VERBOSE


def randint(lowest=1, limit=9999):
    return random.randint(lowest, limit)


def randstr(length=-1):
    alphabet = string.ascii_letters + string.digits
    text = ''
    if length < 10:
        for i in range(randint(100, 200)):
            text += random.choice(alphabet)
    else:
        for i in range(length):
            text += random.choice(alphabet)
    return text


def add(x, y):
    return x + y


def subtract(x, y):
    return x - y


def multiply(x, y):
    return x * y


def modulo(x, y):
    return x % y


def divide_and_floor(x, y):
    return math.floor(x / y)


def xor(x, y):
    return str(int(x) ^ int(y))


def base64encode(x):
    return base64.b64encode(x.encode("utf-8")).decode("utf-8")


def base64_generator():
    x = randstr(200)
    question = f"BASE64 {x}"
    result = base64encode(x)

    return {"question": question, "result": result}


def xor_generator():
    x = randint(100000, 9999999999)
    y = randint(100000, 9999999999)
    question = f"XOR {x} {y}"
    result = xor(x, y)

    return {"question": question, "result": result}


def sum_generator():
    operator = random.choice(['ADD', 'SUBTRACT', 'MULTIPLY', 'DIVIDE AND FLOOR', 'MODULO'])
    x = randint()
    y = randint()
    if x < y:
        temp = x
        x = y
        y = temp

    question = f"{operator} {x} {y}"
    result = ""

    if operator == 'ADD':
        result = f"{add(x, y)}"
    elif operator == 'SUBTRACT':
        result = f"{subtract(x, y)}"
    elif operator == 'MULTIPLY':
        result = f"{multiply(x, y)}"
    elif operator == 'DIVIDE AND FLOOR':
        result = f"{divide_and_floor(x, y)}"
    elif operator == 'MODULO':
        result = f"{modulo(x, y)}"

    return {"question": question, "result": result}


def hash_generator():
    algorithms = list(hashlib.algorithms_guaranteed)

    if 'shake_128' in algorithms:
        algorithms.remove('shake_128')
    if 'shake_256' in algorithms:
        algorithms.remove('shake_256')

    algorithm = random.choice(algorithms)
    text = randstr()

    hash_object = hashlib.new(algorithm)
    hash_object.update(text.encode('utf-8'))

    question = f"{algorithm} {text}"
    result = hash_object.hexdigest()

    return {"question": question, "result": result}


def stages_generator(count):
    stages = {}

    generators = [
        sum_generator,
        hash_generator,
        xor_generator,
        base64_generator
    ]

    if count < len(generators):
        count = len(generators)

    for i in range(count):
        if i < len(generators):
            stages[i] = generators[i]()
        else:
            stages[i] = random.choice(generators)()

        if DEBUG:
            print(i, stages[i])

    if VERBOSE:
        print("stages", stages)

    return stages
