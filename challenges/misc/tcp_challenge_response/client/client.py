import socket
import hashlib
import math
import base64


def add(x, y):
    return x + y


def subtract(x, y):
    return x - y


def multiply(x, y):
    return x * y


def modulo(x, y):
    return x % y


def divide_and_floor(x, y):
    return math.floor(x / y)


def base64decode(x):
    return base64.b64encode(x.encode("utf-8")).decode("utf-8")


def xor(x, y):
    return str(int(x) ^ int(y))


if __name__ == "__main__":

    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Connect the socket to the port where the server is listening
    server_address = ('localhost', 9999)
    print(f"connecting to {server_address[0]} port {server_address[1]}")
    sock.connect(server_address)
    rounds = 0
    try:
        result = ''
        while True:
            rounds += 1
            print('----------------------------------------------\n')
            data = sock.recv(1024).decode('utf-8')
            data = data.split()

            if 'flag' in (' '.join(data).lower()):
                print(' '.join(data))
                print(f"This game lasted {rounds} rounds")
                break

            # 2 items
            elif data[0].lower() == 'base64':
                result = base64decode(data[1])
            elif data[0].lower() in hashlib.algorithms_guaranteed:
                hash_object = hashlib.new(data[0])
                hash_object.update(data[1].encode('utf-8'))
                result = hash_object.hexdigest()

            # 3 items
            elif data[0].lower() == 'add':
                result = add(int(data[1]), int(data[2]))
            elif data[0].lower() == 'subtract':
                result = subtract(int(data[1]), int(data[2]))
            elif data[0].lower() == 'multiply':
                result = multiply(int(data[1]), int(data[2]))
            elif data[0].lower() == 'modulo':
                result = modulo(int(data[1]), int(data[2]))
            elif data[0].lower() == 'xor':
                result = xor(int(data[1]), int(data[2]))

            # 5 items
            elif data[0].lower() == 'divide':
                result = divide_and_floor(int(data[3]), int(data[4]))
                print(result)

            # after result calculation send result to server
            print(result)
            sock.sendall(f"{result}".encode('utf-8'))
            print(sock.recv(1024).decode('utf-8'))
            result = ''

    finally:
        print('closing socket')
        sock.close()
