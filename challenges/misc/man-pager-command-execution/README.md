# [Misc] [Easy] - Man Pager
## Description
Makes use of the !<command> options in the less pages used by the man command
opens man less as soon as ssh connection starts

## Author(s)
- PrincePanda

## How to build/host
- Docker image
- Text instructions on CTFd
- Hint on CTFd

### Docker compile commands
```
sudo docker build -t manrce .
sudo docker run -p 2222:22 -d manrce
```

### Text instructions
> Maybe read up on some man pages? login with `ssh ctf@<url> -p <port>` and the password `ctf`
> I head there's a flag in `/home/ctf/flag.txt`

## Solution
<details>
<summary>Click to show</summary>

Type `!cat /home/ctf/flag.txt`

<pre><code>snippit of code</code></pre>

</details>

### Flag
```PTF{L3Ss_H4se_c0D3_3x3cUT10N}```
