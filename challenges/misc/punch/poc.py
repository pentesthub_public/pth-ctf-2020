#!/usr/bin/python3

from punchlib import decode

def generateSourceFromCards(path):
    order = [9,8,7,6,5,4,3,2,1,0,11,12]
    with open('poc.F95', 'w+') as source:
        for n in range(1,34):
            with open(path+"card"+str(n)+".txt") as card:
                lines = card.readlines()
            
            line = ''
            rotated = [list(r[1:-1]) for r in zip(*lines[::-1])][3:-1]
            
            for cols in range(0, len(rotated)):
                indices = [i for i, x in enumerate(rotated[cols]) if x == "x"]
                line += decode([order[i] for i in indices])
            source.write(line+'\n')

if __name__ == "__main__":
    generateSourceFromCards('cards/')