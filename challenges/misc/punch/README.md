# [Misc] [Medium] - Punch
## Description
Punch cards and fortran program to run

## Author(s)
- RPScylla

## How to build/host
- Text instructions on CTFd
- File on CTFd
- Hint on CTFd

### Text instructions
> We found some old punch cards. Can you figure out what the program does?

### File on CTFd
```
generate using `gen.py`, upload the contents of the cards folder (as zip)
```
- `cards.zip`

### Hint
> 029

## Solution
<details>
<summary>Click to show</summary>
<p>

see `poc.py` for implementation.

</p>
</details>

### Flag
```PTH{d0n7_g3t_punc3d_by_f0rtr4n95}```