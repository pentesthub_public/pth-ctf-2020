#029 __________________________________________________________________
#   |&-0123456789ABCDEFGHIJKLMNOPQR/STUVWXYZ:#@'="¢.<(+|!$*);¬ ,%_>?
# 12|x           xxxxxxxxx                        xxxxxx              
# 11| x                   xxxxxxxxx                     xxxxxx              
#  0|  x                           xxxxxxxxx                   xxxxx              
#  1|   x        x        x        x                                              
#  2|    x        x        x        x       x     x     x                         
#  3|     x        x        x        x       x     x     x     x               
#  4|      x        x        x        x       x     x     x     x                    
#  5|       x        x        x        x       x     x     x     x                 
#  6|        x        x        x        x       x     x     x     x                  
#  7|         x        x        x        x       x     x     x     x              
#  8|          x        x        x        x xxxxxxxxxxxxxxxxxx xxxxx            
#  9|           x        x        x        x                                      
#   |_____________________________________________________________________


def decode(code):
    code.sort()
    if code == [12]: return '&'
    elif code == [11]: return '-'
    elif code == [0]: return '0'
    elif code == [1]: return '1'
    elif code == [2]: return '2'
    elif code == [3]: return '3'
    elif code == [4]: return '4'
    elif code == [5]: return '5'
    elif code == [6]: return '6'
    elif code == [7]: return '7'
    elif code == [8]: return '8'
    elif code == [9]: return '9'
    elif code == [1, 12]: return 'A'
    elif code == [2, 12]: return 'B'
    elif code == [3, 12]: return 'C'
    elif code == [4, 12]: return 'D'
    elif code == [5, 12]: return 'E'
    elif code == [6, 12]: return 'F'
    elif code == [7, 12]: return 'G'
    elif code == [8, 12]: return 'H'
    elif code == [9, 12]: return 'I'
    elif code == [1, 11]: return 'J'
    elif code == [2, 11]: return 'K'
    elif code == [3, 11]: return 'L'
    elif code == [4, 11]: return 'M'
    elif code == [5, 11]: return 'N'
    elif code == [6, 11]: return 'O'
    elif code == [7, 11]: return 'P'
    elif code == [8, 11]: return 'Q'
    elif code == [9, 11]: return 'R'
    elif code == [0, 1]: return '/'
    elif code == [0, 2]: return 'S'
    elif code == [0, 3]: return 'T'
    elif code == [0, 4]: return 'U'
    elif code == [0, 5]: return 'V'
    elif code == [0, 6]: return 'W'
    elif code == [0, 7]: return 'X'
    elif code == [0, 8]: return 'Y'
    elif code == [0, 9]: return 'Z'
    elif code == [2, 8]: return ':'
    elif code == [3, 8]: return '#'
    elif code == [4, 8]: return '@'
    elif code == [5, 8]: return "'"
    elif code == [6, 8]: return '='
    elif code == [7, 8]: return '"'
    elif code == [2, 8, 12]: return '¢'
    elif code == [3, 8, 12]: return '.'
    elif code == [4, 8, 12]: return '<'
    elif code == [5, 8, 12]: return '('
    elif code == [6, 8, 12]: return '+'
    elif code == [7, 8, 12]: return '|'
    elif code == [2, 8, 11]: return '!'
    elif code == [3, 8, 11]: return '$'
    elif code == [4, 8, 11]: return '*'
    elif code == [5, 8, 11]: return ')'
    elif code == [6, 8, 11]: return ';'
    elif code == [7, 8, 11]: return '¬'
    elif code == [0, 2, 8]: return ' '
    elif code == [0, 3, 8]: return ','
    elif code == [0, 4, 8]: return '%'
    elif code == [0, 5, 8]: return '_'
    elif code == [0, 6, 8]: return '>'
    elif code == [0, 7, 8]: return '?'
    else: return '~'

def encode(char):
    if char == '&': return [12]
    elif char == '-': return [11]
    elif char == '0': return [0]
    elif char == '1': return [1]
    elif char == '2': return [2]
    elif char == '3': return [3]
    elif char == '4': return [4]
    elif char == '5': return [5]
    elif char == '6': return [6]
    elif char == '7': return [7]
    elif char == '8': return [8]
    elif char == '9': return [9]
    elif char == 'A': return [1, 12]
    elif char == 'B': return [2, 12]
    elif char == 'C': return [3, 12]
    elif char == 'D': return [4, 12]
    elif char == 'E': return [5, 12]
    elif char == 'F': return [6, 12]
    elif char == 'G': return [7, 12]
    elif char == 'H': return [8, 12]
    elif char == 'I': return [9, 12]
    elif char == 'J': return [1, 11]
    elif char == 'K': return [2, 11]
    elif char == 'L': return [3, 11]
    elif char == 'M': return [4, 11]
    elif char == 'N': return [5, 11]
    elif char == 'O': return [6, 11]
    elif char == 'P': return [7, 11]
    elif char == 'Q': return [8, 11]
    elif char == 'R': return [9, 11]
    elif char == '/': return [0, 1]
    elif char == 'S': return [0, 2]
    elif char == 'T': return [0, 3]
    elif char == 'U': return [0, 4]
    elif char == 'V': return [0, 5]
    elif char == 'W': return [0, 6]
    elif char == 'X': return [0, 7]
    elif char == 'Y': return [0, 8]
    elif char == 'Z': return [0, 9]
    elif char == ':': return [2, 8]
    elif char == '#': return [3, 8]
    elif char == '@': return [4, 8]
    elif char == "'": return [5, 8]
    elif char == '=': return [6, 8]
    elif char == '"': return [7, 8]
    elif char == '¢': return [2, 8, 12]
    elif char == '.': return [3, 8, 12]
    elif char == '<': return [4, 8, 12]
    elif char == '(': return [5, 8, 12]
    elif char == '+': return [6, 8, 12]
    elif char == '|': return [7, 8, 12]
    elif char == '!': return [2, 8, 11]
    elif char == '$': return [3, 8, 11]
    elif char == '*': return [4, 8, 11]
    elif char == ')': return [5, 8, 11]
    elif char == ';': return [6, 8, 11]
    elif char == '¬': return [7, 8, 11]
    elif char == ' ': return [0, 2, 8]
    elif char == ',': return [0, 3, 8]
    elif char == '%': return [0, 4, 8]
    elif char == '_': return [0, 5, 8]
    elif char == '>': return [0, 6, 8]
    elif char == '?': return [0, 7, 8]
    else: return [0,1,2,3,4,5,6,7,8,9,10,11,12]