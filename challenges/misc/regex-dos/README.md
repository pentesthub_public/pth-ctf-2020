# [Misc] [Medium] - Is this regular?
## Description
Regex denial of service by using backtracking

## Author(s)
- PrincePanda

## How to build/host
- Text instructions on CTFd
- Hint on CTFd


### Text instructions
> We have created the ultimate secure database solution!
> It's running on `nc <ip> <port>`

### Hint
> Wonder if the intern finished his job

## Solution
<details>
<summary>Click to show</summary>

insert `xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx` into the database
search for `(x+x+)+y`
see flag in the log

<pre><code>snippit of code</code></pre>

</details>

### Flag
```PTF{Re9EX_DDo5_15_ThE_1NtErN'5_proBlem}```
