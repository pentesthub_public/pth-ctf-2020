#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const int SOLUTION = 55774;
const int MULTIPLIER = 5;

void math_challenge(){
    char read_buff[0xff];
    char *eptr;
    printf("Welcome to the Computer Math! \n");
    printf("----------------------------- \n");
    printf("Solve x for: \n");
    printf("x > %d \n", SOLUTION);
    printf("x * %d =  %d \n", MULTIPLIER, SOLUTION);
    printf("----------------------------- \n");
    printf("Enter the solution for x: ");
    scanf("%s", read_buff);

    unsigned int input_uint = atoi(read_buff);
    if (input_uint > SOLUTION) {
        printf("%d is bigger than %d \n", input_uint, SOLUTION);
    } else {
        printf("WRONG: %d is not bigger than %d \n", input_uint, SOLUTION);
        exit(0);
    }

    if( input_uint * MULTIPLIER == SOLUTION) {
        printf("%d * %d is equal to %d \n", input_uint, MULTIPLIER, SOLUTION);
        system("cat flag");
    } else {
        printf("WRONG: %d * %d is not equal to %d \n", input_uint, MULTIPLIER, SOLUTION);
        exit(0);
    }

}

void main(int argc, char* argv[]){
    setvbuf(stdout, NULL, _IONBF, 0);
    math_challenge();
}