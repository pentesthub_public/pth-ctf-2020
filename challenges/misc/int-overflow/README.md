# [misc] [Easy] - Int Overflow
## Description
Math with a int overflow

## Author(s)
- RPScylla

## How to build/host
- Docker image
- Text instructions on CTFd
- Hint on CTFd

### Docker compile commands
```
MultiStage docker pipeline (source is build in first image)

sudo docker build --rm -t overflow .
sudo docker run -p 1234:1234 --rm -d overflow
```

### Text instructions
> todo

### Hint
> Computers can't count to infinity

## Solution
<details>
<summary>Click to show</summary>
<p>

- overflow = (2^32 + destination) / multiplier
- (2^32 + 55774) / 5 = `859004614`

</p>
</details>

### Flag
```PTH{c0mpu73r5_d0_w31rd_m47h}```
