#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char **argv)
{
  setvbuf(stdout, NULL, _IONBF, 0);
  volatile int modified;
  char buffer[64];

  modified = 0;
  while(1)
  {
    gets(buffer);

    if(modified == 0xdeadbeef) {
        printf("you have correctly got the variable to the right value\n");
        printf("PTH{Th1s_1s_wh@t_w3_CalL_a_8uff3r_0v3Rfl0w}");
    } else {
        printf("Try again, you got 0x%08x\n", modified);
    }
  }
}
