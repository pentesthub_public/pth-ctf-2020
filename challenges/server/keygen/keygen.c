#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

bool check_structure(char s[0xFF])
{
    for (size_t i = 0; i < 24; i++)
    {
        if(i % 5 == 4) {
            if(s[i] != 0x2D) {
                return false;
            }
        } else {
            if(isalnum(s[i]) == 0) {
                return false;
            }
        }
    }

    return true;
}

bool validate_part1(char p1[0x04])
{
    return (
        (p1[0] == 0x61) &&
        ((p1[1] & p1[3]) == 0x61) &&
        ((p1[1] | p1[2]) == 0x77)        
    );
}

bool validate_part2_5(char p2[0x04], char p5[0x04])
{
    return (
        ((p2[0] ^ p5[0]) == 0x51) &&
        ((p2[1] & p5[1]) == 0x40) &&
        ((p2[2] ^ p5[3]) == 0x0f) &&
        ((p2[3] & p5[2]) == 0x24)
    );
}

bool validate_part3_4(char p3[0x04], char p4[0x04])
{
    return (
        ((p3[0] & p4[3]) == 0x44) &&
        ((p3[1] | p4[2]) == 0x79) &&
        ((p3[2] & p4[1]) == 0x10) &&
        ((p3[3] | p4[0]) == 0X77)
    );
}

bool validate_key(char key[0xFF])
{
    if(strlen(key) != 24)
    {
        return false;
    }

    if(!check_structure(key))
    {
        return false;
    }

    char part1[0x04], part2[0x04],part3[0x04], part4[0x04], part5[0x04];
    
    memcpy(part1, &key[0x00], sizeof(part1));
    memcpy(part2, &key[0x05], sizeof(part2));
    memcpy(part3, &key[0x0A], sizeof(part3));
    memcpy(part4, &key[0x0F], sizeof(part4));
    memcpy(part5, &key[0x14], sizeof(part5));

    if(!validate_part1(part1))
    {
        return false;
    }

    if(!validate_part2_5(part2, part5))
    {
        return false;
    }

    if(!validate_part3_4(part3, part4))
    {
        return false;
    }

    return true;    
}


void login(){
    char read_buff[0xFF];
    char key[25];
    printf("Welcome to the firewall! \n");
    printf("----------------------------- \n");
    printf("This product is not activated! \n");
    printf("----------------------------- \n");
    printf("Please enter your key: \n");

    fgets(read_buff, sizeof(read_buff), stdin);
    sscanf(read_buff, "%s", key);

    if( validate_key(key)) {
        printf("Valid key entered \n");
        system("cat flag");
    } else {
        printf("WRONG KEY");
        exit(0);
    }

}

void main(int argc, char* argv[]){
    setvbuf(stdout, NULL, _IONBF, 0);
    login();
}