#!/usr/bin/python
from z3 import Solver, BitVec, Or, And, unsat

s = Solver()
key = [ BitVec('b%i' % i, 32) for i in range(0, 24) ]

p1 = 0
p2 = 5
p3 = 10
p4 = 15
p5 = 20

for i in range(0, 24):
    if(i % 5 == 4):
        s.add(key[i] == 45)
    else:
        s.add(Or(And(key[i] >= 48, key[i] <= 57), And(key[i] >= 65, key[i] <= 90), And(key[i] >= 97, key[i] <= 122)))

s.add(key[p1+0] == 0x61)
s.add((key[p1+1] & key[p1+3]) == 0x61)
s.add((key[p1+1] | key[p1+2]) == 0x77)

s.add((key[p2+0] ^ key[p5+0]) == 0x51)
s.add((key[p2+1] & key[p5+1]) == 0x40)
s.add((key[p2+2] ^ key[p5+3]) == 0x0f)
s.add((key[p2+3] & key[p5+2]) == 0x24)

s.add((key[p3+0] & key[p4+3]) == 0x44)
s.add((key[p3+1] | key[p4+2]) == 0x79)
s.add((key[p3+2] & key[p4+1]) == 0x10)
s.add((key[p3+3] | key[p4+0]) == 0X77)

	
if s.check() != unsat:
    mod = s.model()
    key_plain = ""	
    for i in range(0, 24):
        x = mod[key[i]].as_long()
        key_plain += chr(x)
    print("key: {}".format(key_plain))
	