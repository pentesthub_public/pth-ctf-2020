# [Server] [Medium] - Keygen
## Description
z3 cracking

## Author(s)
- RPScylla

## How to build/host
- Docker image
- File on CTFd
- Text instructions on CTFd
- Hint on CTFd

### Docker compile commands
```
MultiStage docker pipeline (source is build in first image)

sudo docker build --rm -t keygen .
sudo docker run -p 1234:1234 --rm -d keygen
```

### File on CTFd
- `keygen.c` source

### Text instructions
> Can you figure out what a valid key is?

### Hint
> if www = w3 then zzz = ..

## Solution
<details>
<summary>Click to show</summary>
<p>

- `see poc.py for z3 implementation`

valid key: `ag5q-3PZm-LI4w-2X1d-bo6U`
valid key: `aaVa-0HA4-D907-PZPD-aPdN`

</p>
</details>

### Flag
```PTH{y0u_r3v3r53d_7h3_k3y}```
