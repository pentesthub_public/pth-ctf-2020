#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void access_check(){
    char read_buff[0xff];
    setvbuf(stdout, NULL, _IONBF, 0);
    printf("Welcome\n");
    printf("Enter your secret password:\n");
    scanf("%s", read_buff);
    if(strcmp(read_buff, "dont_store_passwords_in_plain_text") == 0){
        printf("access granted\n");
        system("cat flag");
    } else {
        printf("Wrong password!\n");
        scanf("%s", read_buff);
        exit(0);
    }
}

void main(int argc, char* argv[]){
    access_check();
}