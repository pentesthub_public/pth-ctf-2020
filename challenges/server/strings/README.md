# [server] [Easy] - Strings
## Description
Strings to find the secret key hardcoded in the application

## Author(s)
- RPScylla

## ToDo:
- Fix flag
- Fix dialogue in application

## How to build/host
- Docker image
- Text instructions on CTFd
- File on CTFd
- Hint on CTFd

### Docker compile commands
```
MultiStage docker pipeline (source is build in first image)

sudo docker build --rm -t strings .
sudo docker run -p 1234:1234 --rm -d strings
```

### Text instructions
> todo

### File on CTFd
```
Build docker image, extract file
ToDo: describe how
```

### Hint
> programmers store text in `strings`

## Solution
<details>
<summary>Click to show</summary>
<p>

- Get the binary
- Run strings on it
- Grab the secret key
- Connect via nc to challenge server
- Enter key and receive the flag

</p>
</details>

### Flag
```PTH{flag}```
