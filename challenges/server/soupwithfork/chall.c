#include <stdio.h>
#include <unistd.h> 
#include <string.h>
#include <stdlib.h>

int main()
{
    setvbuf(stdout, NULL, _IONBF, 0);

    while(1)
    {
        printf("Soup should be eaten with a?\n");
	    fflush(stdout);
        input();
    }
}

int input()
{
    int pid = fork();
    if (pid == 0)
    {
        char buffer[16];
        int index = 0;
        char c[1];
        while(1)
        {
            scanf("%c", c);
            if (c[0] == '\n') { break; }
            buffer[index++] = c[0];
        }

        if (strncmp(buffer, "fork", 4) != 0)
        {
            printf("No silly\n");
        }   
        else
        {
            printf("Correct!\n");
        }
        fflush(stdout);
    }
    else
    {
        wait(NULL);
    }
}

int flag()
{
    system("cat flag.txt");
    exit(0);
}

