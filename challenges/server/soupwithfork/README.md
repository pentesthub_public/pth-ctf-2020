# [Misc] [Medium] - The smallest challenge
## Description
Python jail -> help() -> pager -> command exec

## Author(s)
- PrincePanda

## How to build/host
- Docker image
- Text instructions on CTFd

### Docker compile commands
```
Docker setup instructions
```

### Text instructions
Byte-at-a-time canary brute forcing


## Solution
<details>
<summary>Click to show</summary>

Run python script
<pre><code>snippit of code</code></pre>

</details>

### Flag
```PTH{w3 eat 0ur c00k13s w1Th a F0rK T00}```
