# [Stego] [Medium] - My favorite dino
## Description
Python bytecode stego

## Author(s)
- PrincePanda

## How to build/host
- Text instructions on CTFd
- File on CTFd
- Hint on CTFd

### Text instructions
> I want to show you my favorite dinosaurs!
> Download this file and tell me what you think about them.
> They would never hide anything from me.


### File on CTFd
```__pycache__/chall.cpython-36.pyc```


### Hint
> I hope you noticed my favorite dino is the stegosaurus.

## Solution
<details>
<summary>Click to show</summary>

Either know that python3.6 and further moved to a 2 byte opcode which allows for injecting data in dead code.
Or just use the stegosaurus tool available on github.....
</details>

### Flag
```PTH{my_own_stego_saurus}```
