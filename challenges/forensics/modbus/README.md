# [Forensics] [Medium] - Going back to the old skool
## Description
Loads of bs traffic with modbus traffic. Modbus master writes the data and then reads it. Every read is a single char of flag and occurs every 6 sec.

## Author(s)
- Matthijs Vogel

## Solution
<details>
<summary>Click to show</summary>

<pre><code>tshark -r modbus_challenge.pcap -Y "modbus.bitval" -T fields -e modbus.bitval | sed 's/,//g'</code></pre>

convert to ascii using CyberChef => flag
</details>

### Flag
```PTH{talking_about_old_protocols}```