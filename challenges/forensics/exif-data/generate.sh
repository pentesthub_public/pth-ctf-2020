#!/bin/sh

exiftool -overwrite_original \
    -rights="©2020 Pentesthub, all rights reserved" \
    -CopyrightNotice="©2020 Pentesthub, all rights reserved" \
    -artist="Scylla" \
    -CreatorCity="Zoetermeer" \
    -CreatorCountry="The Netherlands" \
    -CreatorAddress="Bleiswijkseweg 27" \
    -CreatorPostalCode="2712 PB" \
    -CreatorRegion="Zuid-Holland" \
    -CreatorWorkEmail="ctf@pentesthub.nl" \
    -CreatorWorkTelephone="nvt" \
    -CreatorWorkURL="pentesthub.nl" \
    -Creator="Scylla" \
    -AuthorsPosition="Pentester" \
    -Title="Password" \
    -City="Zoetermeer" \
    -UsageTerms="Don't hack us" \
    -WebStatement="UFRIezN4MWZkNDc0XzE1X3U1M2Z1bGxfZjByXzU3MHIxbjZfcDQ1NXcwcmQ1fQ==" \
    "password.jpg"