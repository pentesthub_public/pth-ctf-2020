# [forensics] [Easy] - Exif Data
## Description
Exif data

## Author(s)
- RPScylla

## ToDo:
- Fix CTFd text

## How to build/host
- File on CTFd
- Text instructions on CTFd
- Hint on CTFd

### File on CTFd
- `password.jpg`

### Text instructions
> We lost the password for our servers. Somewhere in this file it should be.

### Hint
> There is more data in a picture then only pixels

## Solution
<details>
<summary>Click to show</summary>
<p>

ugly oneliner: `exiftool "./password.jpg" | grep Web | cut -d ":" -f2 | awk '{$1=$1};1' | base64 --decode`

</p>
</details>

### Flag
```PTH{3x1fd474_15_u53full_f0r_570r1n6_p455w0rd5}```
