#!/usr/bin/python3

import random, math

with open('flag', 'r') as flagFile:
    flag = flagFile.read()

with open('cave.bmp', 'rb') as orgImage:
    image = bytearray(orgImage.read())

position = math.floor(len(image) / 10)
spacing_from = math.floor(len(image) / 200)
spacing_to = math.floor(len(image) / 100)

for x in flag:
    position += random.randint(spacing_from, spacing_to)
    char = ord(x)
    image[position] = char

with open('cave2.bmp', 'wb+') as newImage:
    newImage.write(image)