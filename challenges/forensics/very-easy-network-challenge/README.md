# [Forensics] [Easy] - Very easy network challenge
## Description
Loads of bs traffic with a simple http retrieval of success.txt file with flag inside of it. Experienced ctf'ers will get this challenge in like 1 min.

## Author(s)
- Matthijs Vogel

## Solution
<details>
<summary>Click to show</summary>

<pre><code>data-text-lines contains "PTH"</code></pre>

</details>

### Flag
```PTH{that_wasnt_so_hard_right}```
