'use strict';

const PORT = 8080;
const HOST = '0.0.0.0';

const express = require('express');
var cookieParser = require('cookie-parser')
const puppeteer = require('puppeteer');
const path = require('path');
const fs = require('fs');

const app = express();

function replaceView(values, content){
    for(var key in values) {
        content = content.replace("{{"+key+"}}", values[key]);
    }
    return content;
}

app.use(cookieParser())
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.use(express.static(__dirname + '/public'));

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + '/home.html'));
})

app.get('/set_cookie', function(req,res){
    res.cookie(req.query.n,req.query.v, { maxAge: 900000, httpOnly: false });
    res.send("set");
})

app.get('/image', function(req,res){
    res.send("<img src='"+req.query.q+"' />");
})

app.post('/', function (req, res) {
    (async () => {
        const browser = await puppeteer.launch({args: ['--no-sandbox']});
        const page = await browser.newPage();
        await page.goto('http://localhost:8080/set_cookie?n=admin&v=PTH%7Bx55_pr073c7_my_c00k135!%7D');
        await page.goto('http://localhost:8080/image?q='+encodeURIComponent(req.body.url));
      
        await browser.close();
      })();
    
    var content = fs.readFileSync(path.join( __dirname, 'image.html'), 'utf8');
    
    content = replaceView({ "src" : req.body.url }, content);

    res.send(content);
})

app.listen(PORT, HOST);