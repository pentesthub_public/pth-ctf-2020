'use strict';

const PORT = 8080;
const HOST = '0.0.0.0';

const express = require('express');
const app = express();

const path = require('path');
const fs = require('fs');

function replaceView(values, content){
    for(var key in values) {
        content = content.replace("{{"+key+"}}", values[key]);
    }
    return content;
}

app.use(express.static(__dirname + '/public'));

app.get('/', function(req, res) {
    var content = fs.readFileSync(path.join( __dirname, 'template.html'), 'utf8');

    var contentPath = (req.query.p === undefined) ? 'home.html' : req.query.p;
    
    var title = contentPath.split('.');
    title = title[title.length -2];
    title = title.charAt(0).toUpperCase() + title.slice(1);

    var pageContent = "";
    if (fs.existsSync(contentPath)) {
        pageContent = fs.readFileSync(path.join(__dirname, contentPath));
    } else {
        var directoryPart = contentPath.split('/');
        directoryPart.pop();

        var directoryPath = path.join(__dirname, directoryPart.join('/'));

        var files = fs.readdirSync(directoryPath);

        files.forEach(function (file) {
            pageContent += file + "<br />";
        });
    }

    content = replaceView({ "title" : title, "content" : pageContent }, content);

    res.send(content);
});

app.listen(PORT, HOST);