<?php
    if(!file_exists("../firewall.db.bak"))
    {
        copy("../firewall.db", "../firewall.db.bak");
    }
    if(isset($_POST['reset']))
    {
        copy("../firewall.db.bak", "../firewall.db");
    }
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin portal</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/sticky-footer-navbar.css" rel="stylesheet">
</head>

<body>

    <header>
        <!-- Fixed navbar -->
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="#">Time machine firewall</a>
        </nav>
    </header>

    <!-- Begin page content -->
    <main role="main" class="container">
        <h1 class="mt-5">Firewall rules</h1>
        <form action='' method="post">
            Filter: <input type='text' name="filter" >
            <input type="submit" class="btn btn-primary">
        </form>
        <form action='' method="post">
            <input class="btn btn-warning" type="submit" name="reset" value="Reset DB">
        </form>
<?php
    $db = new SQLite3('../firewall.db');
    $db->enableExceptions(true);
try {
    $res = $db->query("SELECT `rule`, `port`,`protocol`,`allow` FROM `firewall_rules` WHERE `rule` like '%".$_POST['filter']."%'");
} catch(Exception $e) {
    echo 'Caught exception: ' . $e->getMessage();
}
?>
            
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Rule</th>
                    <th scope="col">port</th>
                    <th scope="col">protocol</th>
                    <th scope="col">allowed</th>
                </tr>
            </thead>
            <tbody>
                <?php
            while ($row = $res->fetchArray()) {
                echo "<tr>";
                echo "<td>".$row['rule']."</td>";
                echo "<td>".$row['port']."</td>";
                echo "<td>".$row['protocol']."</td>";
                echo "<td>".($row['allow'] == 1 ? "Yes" : "No")."</td>";
                echo "</tr>";
            }
                ?>
            </tbody>
        </table>
    </main>

    <footer class="footer">
        <div class="container">
            <span class="text-muted">Time machine firewall</span>
        </div>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="js/jquery-slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>