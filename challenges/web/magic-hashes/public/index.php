<?php
$flag = "PTH{md5_m461c_h45h35}";
$pth_hash = 'PTHAEh9J1';
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin portal</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/sticky-footer-navbar.css" rel="stylesheet">
</head>

<body>

    <header>
        <!-- Fixed navbar -->
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="#">Sitename</a>
        </nav>
    </header>

    <!-- Begin page content -->
    <main role="main" class="container">
        <h1 class="mt-5">Please login</h1>
        <form action="" method="POST">
            <input type="text" name="password" />
            <input type="submit">
        </form>

        <?php
            if(isset($_POST['password']))
            {
                if($_POST['password'] !== $pth_hash && md5($pth_hash) == md5($_POST['password']))
                {
                    echo "<p class='lead'>$flag</p>";
                }
                else
                {
        ?>
                    <p class='lead'>Wrong Password!</p>
                    <p class='lead' style='font-size: 4px;'>
                        if($_POST['password'] !== "<?php echo $pth_hash; ?>" && md5("<?php echo $pth_hash; ?>") == md5($_POST['password']))
                        {
                            echo $flag;
                        }
                    </p>
        <?php
                }

            }
        ?>
    </main>

    <footer class="footer">
        <div class="container">
            <span class="text-muted">Place sticky footer content here.</span>
        </div>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="js/jquery-slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>