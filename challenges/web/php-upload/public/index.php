<?php
    $uploaddir = 'uploads/';
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PentestHub File sharing service</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/sticky-footer-navbar.css" rel="stylesheet">
</head>

<body>

    <header>
        <!-- Fixed navbar -->
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="#">PentestHub File sharing service</a>
        </nav>
    </header>

    <!-- Begin page content -->
    <main role="main" class="container">
        <h1 class="mt-5">Pentest fileshare</h1>

        <p class="lead">Welcome pentest members, you can upload your files here for sharing.<br />Share your link with you fellas after uploading.</p>

        <form method="POST" action="/" autocomplete="off" spellcheck="false" enctype="multipart/form-data">
            <div class="form-group">
                <label for="file">Upload a file:</label>
                <input class="form-control-file" type="file" id="file" name="file">
            </div>
            <button class="btn btn-primary" type="submit">Upload</button>
        </form>

        <?php
        if (isset($_FILES['file']))
        {
            $baseName = basename($_FILES['file']['name']);
            $fileName = str_replace("php", "", $baseName);

            if($baseName != $fileName)
            {
                echo '<div class="alert alert-warning" role="alert">Php detected in filename, censored</div>';
                echo '<p style="display:none;">str_replace("php", "", $baseName)</p>';
            }

            $uploadfile = $uploaddir . sha1_file($_FILES['file']['tmp_name']) . substr($fileName, strpos($fileName, "."));

            if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
                    echo "<p class='lead'>Share link: <a href='$uploadfile'>$uploadfile</a></p>";            
            } else {
                echo '<div class="alert alert-danger" role="alert">Upload failed!</div>"';
            }
        }
        ?>
    </main>

    <footer class="footer">
        <div class="container">
            <span class="text-muted">File sharing service; &copy;PentestHub 2020.</span>
        </div>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="js/jquery-slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>