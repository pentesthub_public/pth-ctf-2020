# Web Easy - Evolution
## Description
Combination challenge.

## Author(s)
- RPScylla

## ToDo:
- Include better flag

## How to build/host
- Docker image
- Text instructions on CTFd
- Hint on CTFd

### Docker compile commands
```
sudo docker build --rm -t evolution .
sudo docker run -d -it --rm -p 8080:80 evoltuion
```

### Text instructions
> Can you make it trough all levels of evolution?

### Hint
> ...

## Solution
<details>
<summary>Click to show</summary>
<p>

1: html comment
2: url paramenter 
3: robots.txt
4: cookie edit
5: change get to post

</p>
</details>

### Flag
PTH{evolution}