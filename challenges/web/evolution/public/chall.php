<?php
session_start();
#---- layer 1 - inspect ----
#params
$chall1_pass = "PTH_R0ck5";
$chall1_session_name = "chall1pass";
$chall1_field_name = "asfh78923";

#check
function check_chall1(){
    global $chall1_pass, $chall1_session_name, $chall1_field_name;

    if(isset($_POST[$chall1_field_name])){
        if($_POST[$chall1_field_name] === $chall1_pass){
            $_SESSION[$chall1_session_name] = true;
        }
    }
    return $_SESSION[$chall1_session_name] === true;
}

#print
function print_chall1(){
    global $chall1_field_name;

    echo "<div class=\"col\">";
    echo "<h1 class=\"mt-5\">Welcome to the first layer!</h1>";
        echo "<form action=\"\" method=\"POST\">";
            echo "<div class=\"form-group\">";
                echo "<label for=\"$chall1_field_name\">Password</label>";
                echo "<input type=\"password\" class=\"form-control\" name=\"$chall1_field_name\" id=\"$chall1_field_name\" aria-describedby=\"$chall1_field_name-help\" placeholder=\"Enter password\">";
                echo "<small id=\"$chall1_field_name-help\" class=\"form-text text-muted\">Can you find the hidden password on the site?</small>";
            echo "</div>";
            echo "<button type=\"submit\" class=\"btn btn-primary\">Submit</button>";
        echo "</form>";
        echo "<!-- PTH_R0ck5 -->";
    echo "</div>";
}

#---- layer 2 - parameter ----
#params
$chall2_session_name = "chall2pass";
$chall2_field_name = "flag";

#check
function check_chall2(){
    global $chall2_session_name, $chall2_field_name;

    if(isset($_GET[$chall2_field_name])){
        $_SESSION[$chall2_session_name] = true;
    }

    return $_SESSION[$chall2_session_name] === true;
}

#print
function print_chall2(){
    global $chall2_field_name;

    echo "<div class=\"col\">";
        echo "<h1 class=\"mt-5\">Welcome to second layer!</h1>";
        echo "<p class=\"lead\">It looks like the \"$chall2_field_name\" parameter isnt set?</p>";
    echo "</div>";
}

#---- layer 3 - robots.txt ----
#params
$chall3_pass = "is_this_a_safe_place_to_store_passwords";
$chall3_session_name = "chall3pass";
$chall3_field_name = "asdf9823";

#check
function check_chall3(){
    global $chall3_pass, $chall3_session_name, $chall3_field_name;

    if(isset($_POST[$chall3_field_name])){
        if($_POST[$chall3_field_name] === $chall3_pass){
            $_SESSION[$chall3_session_name] = true;
        }
    }

    return $_SESSION[$chall3_session_name] === true;
}

#print
function print_chall3(){
    global $chall3_field_name;

    echo "<div class=\"col\">";
        echo "<h1 class=\"mt-5\">Welcome to the third layer!</h1>";
        echo "<form action=\"\" method=\"POST\">";
            echo "<div class=\"form-group\">";
                echo "<label for=\"$chall3_field_name\">Password</label>";
                echo "<input type=\"password\" class=\"form-control\" name=\"$chall3_field_name\" id=\"$chall3_field_name\" aria-describedby=\"$chall3_field_name-help\" placeholder=\"Enter password\">";
                echo "<small id=\"$chall3_field_name-help\" class=\"form-text text-muted\">This layer can only be solved by robots!</small>";
            echo "</div>";
            echo "<button type=\"submit\" class=\"btn btn-primary\">Submit</button>";
        echo "</form>";
    echo "</div>";
}

#---- layer 4 - cookie ----
#params
$chall4_pass = "True";
$chall4_default = "False";
$chall4_session_name = "chall4pass";
$chall4_cookie_name = "Admin";
if(!isset($_COOKIE[$chall4_cookie_name])) {
    setcookie($chall4_cookie_name, $chall4_default, time() + (86400 * 30), "/"); // 86400 = 1 day}
}

#check
function check_chall4(){
    global $chall4_pass, $chall4_session_name, $chall4_cookie_name;

    if(isset($_COOKIE[$chall4_cookie_name])) {
        if($_COOKIE[$chall4_cookie_name] === $chall4_pass){
            $_SESSION[$chall4_session_name] = true;
        }
    }
    return $_SESSION[$chall4_session_name] === true;
}

#print
function print_chall4(){
    echo "<div class=\"col\">";
        echo "<h1 class=\"mt-5\">Welcome to the fourth layer!</h1>";
        echo "<p class=\"lead\">This layer is only accesible for admins!</p>";
        echo "<small class=\"text-muted\">Hint: Enable cookies</small>";
    echo "</div>";
}


#---- layer 5 - POST/GET button ----
#params
$chall5_session_name = "chall5pass";
$chall5_field_name = "984f32d491";

#check
function check_chall5(){
    global $chall5_session_name, $chall5_field_name;

    if(isset($_POST[$chall5_field_name])){
        $_SESSION[$chall5_session_name] = true;
    }

    return $_SESSION[$chall5_session_name] === true;
}

#print
function print_chall5(){
    global $chall5_field_name;

    echo "<div class=\"col\">";
        echo "<h1 class=\"mt-5\">Welcome to the Fifth layer!</h1>";
        echo "<form action=\"\" method=\"GET\">";
            echo "<button type=\"submit\" id=\"$chall5_field_name\" name=\"$chall5_field_name\" value=\"True\" class=\"btn btn-primary\">I WONT POST</button>";
        echo "</form>";
    echo "</div>";
}

#---- layer 6 - flag ----
function print_flag(){
    global $flag;

    echo "<div class=\"col\">";
        echo "<h1 class=\"mt-5\">Welcome to the final layer</h1>";
        echo "<p class=\"lead\">$flag</p>";
    echo "</div>";
}

#---- reset ----
if(isset($_POST['reset'])){
    global $chall1_session_name, $chall2_session_name, $chall3_session_name, $chall4_session_name, $chall5_session_name;
    global $chall4_cookie_name, $chall4_default;

    $_SESSION[$chall1_session_name] = false;
    $_SESSION[$chall2_session_name] = false;
    $_SESSION[$chall3_session_name] = false;
    setcookie($chall4_cookie_name, $chall4_default, time() + (86400 * 30), "/"); // 86400 = 1 day}
    $_SESSION[$chall4_session_name] = false;
    $_SESSION[$chall5_session_name] = false;
}


?>