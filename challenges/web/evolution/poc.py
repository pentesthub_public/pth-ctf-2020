#!/usr/bin/python3

import requests

response = requests.get(
    'http://localhost:8080',
    headers={'referer': 'pentesthub.nl'}
)

print(response.text)